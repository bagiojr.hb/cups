@extends ('layout')


@section ('content3')
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>

                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                Cups
            </div>

            <img src="cup.jpg" height="400" width="500"/>

        </div>

    </div>

    <a href="{{ url('/aboutus') }}">  About Us  </a>
    <a href="{{ url('/contact') }}">  Contact  </a>
@endsection
