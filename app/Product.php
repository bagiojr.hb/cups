<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['systemId','photo','title','description','price','name','stockQuantity','ASL','itemName'];
}
