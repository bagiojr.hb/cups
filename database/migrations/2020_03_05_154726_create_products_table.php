<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('systemId');
            $table->timestamps();
            $table->string('photo');
            $table->string('title');
            $table->text('description');
            $table->integer('price');
            $table->string('name');
            $table->integer('stockQuantity');
            $table->string('ASL');
            $table->string('itemName');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
