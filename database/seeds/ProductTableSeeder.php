<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
          'systemId' => 9,
          'photo' => 'frenchcoffee.jpg',
          'title' => 'Latte',
          'description' => 'French Coffee',
          'price' => 100,
          'Name' => 'French Vanilla',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();

        $product = new \app\Product([
          'systemId' => 2,
          'photo' => 'cappo.jpg',
          'title' => 'Cappuccino',
          'description' => 'Italian Coffee',
          'price' => 120,
          'Name' => 'italian Style',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();

        $product = new \app\Product([
          'systemId' => 3,
          'photo' => 'espresso.jpg',
          'title' => 'Espresso',
          'description' => 'Italian Origin',
          'price' => 80,
          'Name' => 'Express Yourself',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();

        $product = new \app\Product([
          'systemId' => 4,
          'photo' => 'flatwhite.jpg',
          'title' => 'Flat White',
          'description' => 'Coffee Drink',
          'price' => 90,
          'Name' => 'Flat White Coffee',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();

        $product = new \app\Product([
          'systemId' => 5,
          'photo' => 'mocha.jpg',
          'title' => 'Mocha',
          'description' => 'Caffe Mocha',
          'price' => 110,
          'Name' => 'Mocha',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();

        $product = new \app\Product([
          'systemId' => 6,
          'photo' => 'irish.jpg',
          'title' => 'Irish Coffee',
          'description' => 'hot coffee, irish whiskey, sugar with cream',
          'price' => 150,
          'Name' => 'Irish Whiskey',
          'stockQuantity' => 50,
          'ASL' => 'cup.jpg',
          'itemName' => 'Wake Up!'
        ]);
        $product->save();
    }
}
